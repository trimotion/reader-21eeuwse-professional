ROUTING
- /
- /chapter-id/chapter-block-id

Op de homepage en overzicht van alle chapters die doorlinken naar /chapter-id/chapter-block-id.

Vervolgens vorige en volgende kunnen doen tussen pagina's:
- /chapter-id/chapter-block-id-1
- /chapter-id/chapter-block-id-2
- /chapter-id/chapter-block-id-3
- /chapter-id/chapter-block-id-4

- /chapter-id-1/chapter-block-id-1
- /chapter-id-1/chapter-block-id-2
- /chapter-id-1/chapter-block-id-3
- /chapter-id-1/chapter-block-id-4