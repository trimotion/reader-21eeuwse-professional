// pages/_document.js
import Document, { Html, Head, Main, NextScript } from "next/document";

import { createResolver } from "next-slicezone/resolver";

import { GA_TRACKING_ID } from '@components/utils/gTag';

const prismicRepoName = process.env.PRISMIC_REPO_NAME

export default class extends Document {
	static async getInitialProps(ctx) {
		const initialProps = await Document.getInitialProps(ctx)
		/* In development, generate an sm-resolver.js file
		that will map slices to components */
		if (process.env.NODE_ENV === 'development') {
			await createResolver()
		}
		return { ...initialProps }
	}

  render() {
    return (
      <Html>
        <Head>
			<script async defer src={`//static.cdn.prismic.io/prismic.js?repo=${prismicRepoName}&new=true`} />
			<script
				async
				src={`https://www.googletagmanager.com/gtag/js?id=${GA_TRACKING_ID}`}
			/>

			<script
				dangerouslySetInnerHTML={{
					__html: `
						window.dataLayer = window.dataLayer || [];
						function gtag(){dataLayer.push(arguments);}
						gtag('js', new Date());
						gtag('config', '${GA_TRACKING_ID}', {
						page_path: window.location.pathname,
						});
					`,
				}}
			/>
		</Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
};
