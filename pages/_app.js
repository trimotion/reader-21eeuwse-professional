import "../styles/main.scss";
import { useState, useEffect } from "react"

import { AnimatePresence } from "framer-motion";

function MyApp({ Component, pageProps, router }) {
    const [isFirstMount, setIsFirstMount] = useState(true);

    useEffect(() => {
        const handleRouteChange = () => {
            isFirstMount && setIsFirstMount(false);
        };

        router.events.on("routeChangeStart", handleRouteChange);

        // If the component is unmounted, unsubscribe
        // from the event with the `off` method:
        return () => {
            router.events.off("routeChangeStart", handleRouteChange);
        };
    }, []);

    return (
		<AnimatePresence exitBeforeEnter>
			<Component  {...pageProps} key={router.route} />
		</AnimatePresence>
    );
}

export default MyApp;
