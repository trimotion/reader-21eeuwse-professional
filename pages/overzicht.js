import { Fragment, useContext, useEffect, useState } from 'react'
import { Client } from "../prismic-configuration";
import Prismic from "@prismicio/client";

// LIBRARIES
import { CarouselProvider, CarouselContext } from 'pure-react-carousel';
import { motion } from 'framer-motion'

// ANIMATION
import InitialTransition from '@components/animation/initialTransition'

// COMPONENTS
import { RichText } from "prismic-reactjs";
import CarouselChapters from '@components/carousel/chapters'
import Body from "@components/body";
import MainNavigation from '@components/navigation/main';
import Head from '@components/head'

const Page = (props, { isFirstMount }) => {
	
	const pageData = props.doc

    return (
        <Fragment>
			{/* <InitialTransition /> */}
			<Head 
				title={pageData.data.title}
				description={pageData.data.description}
			/>
			<CarouselProvider
				naturalSlideWidth={100}
				naturalSlideHeight={100}
				totalSlides={pageData.data.chapters_list.length}
				orientation="horizontal"
				isIntrinsicHeight={true}
				lockOnWindowScroll={true}
				// currentSlide={this.state.currentSlide}
			>
				<MainNavigation />
				<CarouselChapters chapterList={pageData.data.chapters_list} />
				<Body body={pageData.body} />
			</CarouselProvider>
			
        </Fragment>
    );
};





// Fetch content from prismic
export async function getStaticProps({}) {
    const doc = await Client().getSingle("overview");
    const chapters = await Client().query(
        Prismic.Predicates.at("document.type", "chapter")
    );

    return {
        props: {
            doc,
			chapters
        },
    };
}

export default Page;


