import { Fragment, useState, useEffect } from 'react'
import { useRouter } from 'next/router'

// LIBRARIES
import { motion } from "framer-motion";

// PRISMIC
import { Client } from "@base/prismic-configuration"
import Prismic from "@prismicio/client"
import { RichText } from 'prismic-reactjs'

// COMPONENTS
// import useUpdatePreviewRef from "@components/utils/useUpdatePreviewRef"
import Head from "@components/head"
import Body from "@components/body"
import Container from '@components/layout/page/container'
import PageNavigation from '@components/navigation/page'
import Header from '@components/layout/page/header'
import QuestionContainer from '@components/layout/page/question'
import ExitPreviewButton from "@components/utils/exitPreviewButton"
import { yellowTheme, blueTheme, pinkTheme, purpleTheme } from '@components/themes'
import NextPage from '@components/layout/page/nextPage'

const variants = {
    initial: {
        clipPath: 'polygon(0 0, 0 0, 0 100%, 0% 100%)',
		fill: 'red'
    },
    animate: {
        clipPath: 'polygon(0 0, 100% 0, 100% 100%, 0 100%)',
		fill: 'red'
    },
    exit: {
        clipPath: 'polygon(100% 0, 100% 0, 100% 100%, 100% 100%)',
		fill: 'red'
    }
}



const Chapter = ({
	doc, 
	chapter,
	overview,
	nextItem,
	previousItem, 
	preview,
	currentIndex,
	activeTheme,
	nextChapter,
	previousChapter,
	activeChapter,
	currentChapterIndex
}) => {
		const router = useRouter()

		
		
		if (router.isFallback) {
			return <div>Loading...</div>
		}

		const spring = {
			type: "spring",
			damping: 50,
			stiffness: 10,
			when: "afterChildren"
		};
		
		const chapterData = doc.data;
		

		const bgThemeSwitch = () => {
			switch (activeTheme) {
				case 'yellow':
					return `${yellowTheme.backgroundColor}`
				case 'blue':
					return `${blueTheme.backgroundColor}`
				case 'pink':
					return `${pinkTheme.backgroundColor}`
				case 'purple':
					return `${purpleTheme.backgroundColor}`
			}
		}

		const titleThemeSwitch = () => {
			switch (activeTheme) {
				case 'yellow':
					return `${yellowTheme.cardColor}`
				case 'blue':
					return `${blueTheme.cardColor}`
				case 'pink':
					return `${pinkTheme.cardColor}`
				case 'purple':
					return `${purpleTheme.cardColor}`
			}
		}

		console.log(nextItem)

		return (
			<Fragment>
				<Head
					title={chapterData.title}
					description={chapterData.description}
				/>
				{preview.isActive ? <ExitPreviewButton /> : null}
				
				<PageNavigation 
					prevUrl={previousItem} 
					prevChapterUrl={previousChapter}
					nextUrl={nextItem} 
					nextChapterUrl={nextChapter}
					activeChapter={currentIndex}
					firstChapter={currentChapterIndex}
					mainChaptersLength={overview.data.chapters_list.length}
					progressLength={chapter.data.chapter_list.length}
					progressCurrent={currentIndex}
					bgThemeColor={bgThemeSwitch()}
					titleThemeColor={titleThemeSwitch()}
				/>	
				<div style={{ backgroundColor: '#ffffff' }}>
					<Header
						title={chapterData.title}
						description={chapterData.description}
						mainChapter={currentChapterIndex}
						activeSubChapter={currentIndex}
						chapter_title={chapter.data.title}
						reading_time={chapterData.reading_time_in_minutes}
						src={chapter.data.image.url}
						alt={chapter.data.image.alt}
						bgThemeColor={bgThemeSwitch()}
						titleThemeColor={titleThemeSwitch()}
					/>
					
					<Container>
						<p className="reading-time" style={{ color: `${titleThemeSwitch()}` }}>
							{RichText.asText(chapter.data.title)} - Leestijd {chapterData.reading_time_in_minutes} minuten
						</p>
						<Body content={chapterData.body1} titleThemeColor={titleThemeSwitch()} />
					</Container>
					<QuestionContainer bgThemeColor={bgThemeSwitch()}>
						<Body content={chapterData.body} />
					</QuestionContainer>
					<NextPage nextUrl={nextItem}  />
				</div>
			</Fragment>
		);
};

export async function getStaticProps({ params, preview, previewData }) {
    
	const ref = previewData ? previewData.ref : null;
	const isPreview = preview || false;
    
	const doc = await Client().getByUID("chapter_block", params.uid, ref ? { ref } : null) || {};
    const chapter = await Client().getByUID("chapter", params.chapter);
	const overview = await Client().getSingle("overview")

	let activeTheme = null,
		nextChapter = null,
		previousChapter = null,
		activeChapter = null,
		currentChapterIndex = null;
	
	overview.data.chapters_list.forEach((item, index) => {
		if (item.chapter.id === chapter.id) {
			activeTheme = item.select_theme
			currentChapterIndex = index + 1
			if (overview.data.chapters_list[index - 1]) {
                previousChapter = overview.data.chapters_list[index - 1].chapter_block;
            }
            if (overview.data.chapters_list[index + 1]) {
                nextChapter = overview.data.chapters_list[index + 1].chapter_block;
            }
		}
	})
	

	let activeItem = null,
		nextItem = null,
		previousItem = null,
		currentIndex = null;
	
    chapter.data.chapter_list.forEach((item, index) => {
        if (item.chapter_link.id === doc.id) {
            activeItem = item.chapter_link;
			currentIndex = index + 1
            if (chapter.data.chapter_list[index - 1]) {
                previousItem = chapter.data.chapter_list[index - 1].chapter_link;
            }
            if (chapter.data.chapter_list[index + 1]) {
                nextItem = chapter.data.chapter_list[index + 1].chapter_link;
            }
        }
    });

    return {
        props: {
            doc, 
			chapter,
			overview,
			activeItem,
			nextItem,
			previousItem,
			currentIndex,
			activeTheme,
			nextChapter,
			previousChapter,
			activeChapter,
			currentChapterIndex,
            preview: {
                isActive: isPreview,
                activeRef: ref,
            },
        },
    };
}

export async function getStaticPaths() {
	const docs = await Client().query(
		Prismic.predicates.at('document.type', 'chapter_block')
	)
	return {
		paths: docs.results.map((doc) => {
			return {
				params: {
					uid: doc.uid,
					chapter: doc.data.chapter.uid || null
				}
			};
		}),
		fallback: true,
	};
}

export default Chapter;
