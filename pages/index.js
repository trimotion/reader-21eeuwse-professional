import { Fragment, useContext, useEffect, useState } from 'react'
import { Client } from "../prismic-configuration";
import Prismic from "@prismicio/client";

// LIBRARIES
import { motion } from 'framer-motion'

// ANIMATION
import InitialTransition from '@components/animation/initialTransition'

// COMPONENTS
import { RichText } from "prismic-reactjs";
import Image from 'next/image'
import Link from 'next/link'
import MainNavigation from '@components/navigation/main';
import Head from '@components/head'
import { IconRight } from '@components/icons'

const Page = (props) => {
	
	const pageData = props.doc

    return (
        <Fragment>
			<Head 
				title={pageData.data.title}
				description={pageData.data.description}
			/>
			<MainNavigation />
			<div className="flex flex-column main-home">
				<section className="container-home-outer">
					<div className="container-home bg-yellow">
						<div className="img-home w-half">
							<Image
								alt="Afbeelding introductie"
								src="https://images.prismic.io/reader-21eeuwse-professional/d7729e29-d2c7-4510-a576-51fc8e222d3a_Stap+0+-+intro.png?auto=format"
								layout="responsive"
								width="250px"
								height="250px"
								objectFit="contain"
								quality={80}
								priority={true}
							/>
						</div>
						<div className="w-half container-home-text-block">
							<h1 className="home-title">
								Introductie
							</h1>
							<div>
								{pageData.data.chapters_list.map((item, i) => {
									return (
										<div key={i}>
											<motion.div className="button-home flex align-middle flex-between" whileHover={{ scale: 1.05 }}>
												<Link
													href={
														`${item.chapter.uid}` +
														"/" +
														`${item.chapter_block.uid}`
													}
													passHref
												>
													<div className="button-home-text">
														<span>{i + 1}. {RichText.asText(item.chapter_title)}</span>
														<div>{item.chapter_description}</div>
													</div>
												</Link>
												<div>
													<IconRight className="icon-m" />
												</div>
											</motion.div>
										</div>
									)
								}).slice(0,1)}
							</div>
						</div>
					</div>
				</section>
				<section className="container-home-outer">
					<div className="container-home bg-pink">
						<div className="img-home w-half">
							<Image
								alt="Afbeelding hoe kan jij..."
								src="https://images.prismic.io/reader-21eeuwse-professional/183a768a-a5f8-4b0e-ab90-d6670a11e85d_Stap+2+-+v2.png?auto=format"
								layout="responsive"
								width="500px"
								height="500px"
								objectFit="contain"
								quality={80}
								priority={true}
							/>
						</div>
						<div className="w-half flex flex-column self-center">
							<h1 className="home-title">
								Hoe kan jij...
							</h1>
							<div>
								{pageData.data.chapters_list.map((item, i) => {
									return (
										<div key={i}>
											<motion.div className="button-home flex align-middle flex-between" whileHover={{ scale: 1.05 }}>
												<Link
													href={
														`${item.chapter.uid}` +
														"/" +
														`${item.chapter_block.uid}`
													}
													passHref
												>
													<div className="button-home-text">
														<span>{i + 1}. {RichText.asText(item.chapter_title)}</span>
														<div>{item.chapter_description}</div>
													</div>
												</Link>
												<div>
													<IconRight className="icon-m" />
												</div>
											</motion.div>
										</div>
									)
								}).slice(1,3)}
							</div>
						</div>
					</div>
				</section>
				<section className="container-home-outer">
				<div className="container-home bg-blue">
						<div className="img-home w-half">
							<Image
								alt="Afbeelding ...voor jouw omgeving..."
								src="https://images.prismic.io/reader-21eeuwse-professional/3b4497ee-2476-4980-8a8b-14ea0d60931e_Stap+2+-+v1.png?auto=format"
								layout="responsive"
								width="500px"
								height="500px"
								objectFit="contain"
								quality={80}
								priority={true}
							/>
						</div>
						<div className="w-half flex flex-column self-center">
							<h1 className="home-title">
								...voor jouw omgeving...
							</h1>
							<div>
								{pageData.data.chapters_list.map((item, i) => {
									return (
										<div key={i}>
											<motion.div className="button-home flex align-middle flex-between" whileHover={{ scale: 1.05 }}>
												<Link
													href={
														`${item.chapter.uid}` +
														"/" +
														`${item.chapter_block.uid}`
													}
													passHref
												>
													<div className="button-home-text">
														<span>{i + 1}. {RichText.asText(item.chapter_title)}</span>
														<div>{item.chapter_description}</div>
													</div>
												</Link>
												<div>
													<IconRight className="icon-m" />
												</div>
											</motion.div>
										</div>
									)
								}).slice(3,6)}
							</div>
						</div>
					</div>
				</section>
				<section className="container-home-outer">
				<div className="container-home bg-purple">
						<div className="img-home w-half">
							<Image
								alt="Afbeelding betekenisvolle waarde creëeren."
								src="https://images.prismic.io/reader-21eeuwse-professional/c9bbf5dd-62eb-4508-b153-73f0119df43f_Stap+3+-+v1.png?auto=format"
								layout="responsive"
								width="500px"
								height="500px"
								objectFit="contain"
								quality={80}
								priority={true}
							/>
						</div>
						<div className="w-half flex flex-column self-center">
							<h1 className="home-title" style={{ color: '#fff'}}>
								betekenisvolle waarde creëeren.
							</h1>
							<div>
								{pageData.data.chapters_list.map((item, i) => {
									return (
										<div key={i}>
											<motion.div className="button-home flex align-middle flex-between" whileHover={{ scale: 1.05 }}>
												<Link
													href={
														`${item.chapter.uid}` +
														"/" +
														`${item.chapter_block.uid}`
													}
													passHref
												>
													<div className="button-home-text">
														<span>{i + 1}. {RichText.asText(item.chapter_title)}</span>
														<div>{item.chapter_description}</div>
													</div>
												</Link>
												<div>
													<IconRight className="icon-m" />
												</div>
											</motion.div>
										</div>
									)
								}).slice(6,8)}
							</div>
						</div>
					</div>
				</section>
			</div>
        </Fragment>
    );
};





// Fetch content from prismic
export async function getStaticProps({}) {
    const doc = await Client().getSingle("overview");
    const chapters = await Client().query(
        Prismic.Predicates.at("document.type", "chapter")
    );

    return {
        props: {
            doc,
			chapters
        },
    };
}

export default Page;


