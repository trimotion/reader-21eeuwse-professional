import Prismic from "prismic-javascript";
import Link from "next/link";

import smConfig from "./sm.json";


if (!process.env.PRISMIC_REPO_URL) {
  console.warn("Looks like Slice Machine hasn't been bootstraped already.\nCheck the `Getting Started` section of the README file :)");
}

export const apiEndpoint = process.env.PRISMIC_REPO_URL;

// -- Access Token if the repository is not public
// Generate a token in your dashboard and configure it here if your repository is private
export const accessToken = process.env.PRISMIC_REPO_ACCESS_TOKEN;



// -- Link resolution rules
// Manages the url links to internal Prismic documents
export const linkResolver = (doc) => {
	if (doc.type === "chapter_block") {
		return `/${doc.data.chapter.uid}/${doc.uid}`
	}
	return "/";
};

export const customLink = (type, element, content, children, index) => (
  <Link
    key={index}
    href={linkResolver(element.data)}
    as={linkResolver(element.data)}
  >
    <a>{content}</a>
  </Link>
);

export const Router = {
	routes: [
		{
			"type":"chapter_block",
			"path":"/:chapter/:uid",
			resolvers: {
				 chapter: 'chapter'
			}
		}
	],
	href: (type) => {
		const route = Router.routes.find(r => r.type === type);
		return route && route.href;
	}
};

export const Client = (req = null, options = {}) => (
	Prismic.client(apiEndpoint, Object.assign({ routes: Router.routes, ...options }))
);
