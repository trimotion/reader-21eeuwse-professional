import React from "react";
import { array, shape } from "prop-types";
import { RichText } from "prismic-reactjs";

const TextParagraph = ({ slice }) => {
    

	const { items, primary } = slice
	console.log(items);
    return (
        <section>
            {RichText.asText(slice.primary.text)}
        </section>
    );
};

TextParagraph.propTypes = {
    slice: shape({
        primary: shape({
            title: array.isRequired,
        }).isRequired,
    }).isRequired,
};

export default TextParagraph;
