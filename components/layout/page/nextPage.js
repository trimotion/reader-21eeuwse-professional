import { IconRight } from '@components/icons'
import Link from 'next/link'
import React, { Fragment } from 'react'

const NextPage = ({nextUrl}) => {

	const NextPageButton = () => {
		if (nextUrl === null) {
			return null
		} else {
			return (
				<Link href={`${nextUrl.url}`} passHref>		
					<div className="container-nextpage mx-auto text-center">
						Volgende pagina
						<IconRight className="icon-m" />
					</div>
				</Link>
			)
		}
	}

	return (
		<Fragment>
			{NextPageButton()}
		</Fragment>
	)
}

export default NextPage
