import React, { Fragment, useState, useEffect } from 'react'

// LIBRARIES
import { motion, useViewportScroll, useSpring, useTransform } from "framer-motion"

let easing = [0.175, 0.85, 0.42, 0.96];

const textVariants = {
	exit: { y: 100, opacity: 0, transition: { duration: 0.5, ease: easing } },
	enter: {
		y: 0,
		opacity: 1,
		transition: { delay: 0.1, duration: 0.5, ease: easing }
	}
};

const Container = ({children}) => {
	return (
		<motion.div className="container-outer" initial="exit" animate="enter" exit="exit">
			<motion.div className="container" variants={textVariants} >
				<div className="container-sidebar">
					<CircleIndicator style={{ zIndex: 1 }} />
				</div>
				<div 
					className="container-inner" 
				>
					{children}
				</div>
			</motion.div>
		</motion.div>
	)
}


const CircleIndicator = () => {
	const [isComplete, setIsComplete] = useState(false);
	const { scrollYProgress } = useViewportScroll();
	const yRange = useTransform(scrollYProgress, [0, 0.9], [0, 1]);
	const pathLength = useSpring(yRange, { stiffness: 400, damping: 90 });

	useEffect(() => yRange.onChange(v => setIsComplete(v >= 1)), [yRange]);

	return (
		<Fragment>
			<svg className="progress-icon" viewBox="0 0 60 60" style={{width: '60px', position: 'fixed'}}>
				<motion.path
					fill="none"
					strokeWidth="5"
					stroke="black"
					strokeDasharray="0 1"
					d="M 0, 20 a 20, 20 0 1,0 40,0 a 20, 20 0 1,0 -40,0"
					style={{
						pathLength,
						rotate: 90,
						translateX: 5,
						translateY: 5,
						scaleX: -1 // Reverse direction of line animation
					}}
				/>
				<motion.path
					fill="none"
					strokeWidth="5"
					stroke="black"
					d="M14,26 L 22,33 L 35,16"
					initial={false}
					strokeDasharray="0 1"
					animate={{ pathLength: isComplete ? 1 : 0 }}
				/>
			</svg>
		</Fragment>
	)
}

export default Container
