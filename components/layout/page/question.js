import React, { Fragment } from 'react'

const QuestionContainer = ({children, bgThemeColor}) => {
	return (
		<Fragment>
			<div className="question-container flex justify-center" style={{ backgroundColor: `${bgThemeColor}`}}>
				<div className="question-container-inner flex justify-center">
					{children}
				</div>
			</div>
		</Fragment>
	)
}

export default QuestionContainer
