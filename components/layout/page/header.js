import React from 'react'

// LIBRARIES
import Image from 'next/image'
import { motion, AnimatePresence } from 'framer-motion'

// COMPONENTS
import { RichText } from 'prismic-reactjs'

let easing = [0.175, 0.85, 0.42, 0.96];

const textVariants = {
	exit: { y: 100, opacity: 0, transition: { duration: 0.5, ease: easing } },
	enter: {
		y: 0,
		opacity: 1,
		transition: { delay: 0.1, duration: 0.5, ease: easing }
	}
};

const Header = ({ 
		title = [], 
		description, 
		chapter_title = [],
		alt, 
		src, 
		bgThemeColor, 
		titleThemeColor,
		activeSubChapter,
		mainChapter
	}) => {
	return (

		<div className="header" style={{ backgroundColor: `${bgThemeColor}`}}>
			<motion.div className="header-inner" initial="exit" animate="enter" exit="exit">
				<div>
					<motion.h1 className="header-title" variants={textVariants}>
						{mainChapter}.{activeSubChapter}. {RichText.asText(title)}
					</motion.h1>
				</div>
				{/* <motion.div 
					variants={textVariants}
				>
					<Image
						alt={alt}
						src={src}
						layout="responsive"
						width="250px"
						height="200px"
						objectFit="fit"
						quality={80}
						priority={true}
						className=""
					/>
				</motion.div> */}
			</motion.div>
		</div>

	)
}

export default Header
