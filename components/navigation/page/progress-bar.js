import React, { Fragment } from 'react'

const ProgressBar = ({progressLength, progressCurrent, titleThemeColor}) => {

	const progressWidth = 100 / progressLength
	const progressTotal = progressWidth * progressCurrent

	return (
		<Fragment>
			<div className="progress-container">
				<div className="progress-container-inner">
					<div className="progress-bar">
						<div className="progress-fill-page" style={{ width: `${progressTotal}` + '%', position: 'relative', transition: '200ms ease-out', backgroundColor: `${titleThemeColor}` }}>
							<div className="progress-bar-tooltip" style={{ backgroundColor: `${titleThemeColor}` }}>
								<div>
									<span style={{ transition: '200ms ease-out' }}>{progressCurrent}</span> / {progressLength}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</Fragment>
	)
}

export default ProgressBar;

