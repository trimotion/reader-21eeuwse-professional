import React, { Fragment } from 'react'

//  LIBRARIES
import Link from "next/link"
import { motion } from 'framer-motion'

// COMPONENTS
import { IconRight } from '@components/icons'

const NextButton = ({ url, chapterUrl, activeChapter, firstChapter, progressLength, mainChaptersLength, bgThemeColor }) => {
	// console.log(firstChapter === mainChaptersLength && activeChapter < progressLength)
	// console.log(activeChapter)
	const nextButton = () => {
		if (firstChapter === mainChaptersLength && activeChapter === progressLength) {
			return (
				<Fragment>
					<Link href="#" passHref>
						<motion.button className="button bg-black visibility-hidden" whileHover={{ scale: 1.15 }}>
							<IconRight className="icon-m fill-l-yellow" style={{ fill: `${bgThemeColor}` }} />
						</motion.button>
					</Link>
				</Fragment>
			)
		} else if (activeChapter < mainChaptersLength && activeChapter === progressLength) {
			return (
				<Fragment>
					<Link href={`${chapterUrl.url}`} passHref>
						<button className="button bg-black">
							<span style={{ color: `${bgThemeColor}`, paddingLeft: '8px' }}>Volgende hoofdstuk</span> <IconRight className="icon-m fill-l-yellow" style={{ fill: `${bgThemeColor}` }} />
						</button>
					</Link>
				</Fragment>
			)
		} else if (activeChapter < progressLength) {
			return (
				<Fragment>
					<Link href={url.url} passHref>
						<motion.button className="button bg-black" whileHover={{ scale: 1.15 }}>
							<IconRight className="icon-m fill-l-yellow" style={{ fill: `${bgThemeColor}` }} />
						</motion.button>
					</Link>
				</Fragment>
			)
		} else if (firstChapter === mainChaptersLength && activeChapter < progressLength) {
			return (
				<Fragment>
					<Link href={url.url} passHref>
						<motion.button className="button bg-black" whileHover={{ scale: 1.15 }}>
							<IconRight className="icon-m fill-l-yellow" style={{ fill: `${bgThemeColor}` }} />
						</motion.button>
					</Link>
				</Fragment>
			)
		}
	}

	return (
		<Fragment>
			{nextButton()}
		</Fragment>
	)
}

export default NextButton
