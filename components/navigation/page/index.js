import React, { Fragment } from 'react'
import Link from "next/link";

// LIBRARIES
import { motion } from 'framer-motion'

// COMPONENTS
import { IconHamburger } from '@components/icons'
import ProgressBar from './progress-bar';
import NextButton from './next-btn';
import PreviousButton from './prev-btn';

const PageNavigation = ({
		prevUrl, 
		prevChapterUrl, 
		nextUrl, 
		nextChapterUrl, 
		activeChapter, 
		firstChapter, 
		progressCurrent, 
		progressLength,
		mainChaptersLength, 
		bgThemeColor, 
		titleThemeColor
	}) => {

	return (
		<Fragment>
			<div className="flex flex-column-reverse fixed z-index">
				<div className="page-nav-container relative" style={{ backgroundColor: `${bgThemeColor}` }}>
					<div className="page-nav-inner flex flex-between align-middle">
						<PreviousButton
							url={prevUrl}
							chapterUrl={prevChapterUrl} 
							activeChapter={activeChapter} 
							firstChapter={firstChapter} 
							bgThemeColor={bgThemeColor} 
						/>
						<Link href="/overzicht" passHref>
							<motion.button className="button bg-black" whileHover={{ scale: 1.15 }}>
								<IconHamburger className="icon-m" style={{ fill: `${bgThemeColor}` }}/>
							</motion.button>
						</Link>
						<NextButton 
							url={nextUrl} 
							chapterUrl={nextChapterUrl} 
							activeChapter={activeChapter} 
							mainChaptersLength={mainChaptersLength} 
							progressLength={progressLength} 
							firstChapter={firstChapter} 
							bgThemeColor={bgThemeColor} 
						/>
					</div>
				</div>
				<ProgressBar progressLength={progressLength} progressCurrent={progressCurrent} titleThemeColor={titleThemeColor} />
			</div>
		</Fragment>
	)
}

export default PageNavigation