import React, { Fragment } from 'react'

// LIBRARIES
import Link from "next/link"
import { motion } from 'framer-motion'

// COMPONENTS
import { IconLeft } from '@components/icons'

const PreviousButton = ({ url, chapterUrl, activeChapter, firstChapter, bgThemeColor }) => {

	const prevButton = () => {
		if (firstChapter === 1 && activeChapter === 1) {
			return (
				<Fragment>
					<Link href="#" passHref>
						<motion.button className="button bg-black visibility-hidden" whileHover={{ scale: 1.15 }}>
							<IconLeft className="icon-m" style={{ fill: `${bgThemeColor}` }} />
						</motion.button>
					</Link>
				</Fragment>
			)
		} else if (activeChapter === 1) {
			return (
				<Fragment>
					<Link href={`${chapterUrl.url}`} passHref>
						<button className="button bg-black">
							<IconLeft className="icon-m" style={{ fill: `${bgThemeColor}` }} /> <span style={{ color: `${bgThemeColor}`, paddingRight: '8px' }}>Vorige hoofdstuk</span>
						</button>
					</Link>
				</Fragment>
			)
		} else if (activeChapter > 1) {
			return (
				<Fragment>
					<Link href={url.url} passHref>
						<motion.button className="button bg-black" whileHover={{ scale: 1.15 }}>
							<IconLeft className="icon-m" style={{ fill: `${bgThemeColor}` }} />
						</motion.button>
					</Link>
				</Fragment>
			)
		}
	}
	return (
		<Fragment>
			{prevButton()}
		</Fragment>
	)
}

export default PreviousButton
