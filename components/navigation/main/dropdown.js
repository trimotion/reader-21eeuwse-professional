import React, { useState, useRef, useEffect } from 'react'

// HOOKS
import { useDetectOutsideClick } from '@components/utils/useDetectOutsideClick'

// COMPONENTS
import { IconDown } from '@components/icons'

const DropdownMenu = () => {

	const dropdownRef = useRef(null)
	const [ isActive, setIsActive ] = useDetectOutsideClick(dropdownRef, false)
	const onClick = () => setIsActive(!isActive)

	return (
		<div className="dropdown-container">
			<button onClick={onClick} className="dropdown-trigger">
				<span>Intro</span>
				<div className="bg-black">
					<IconDown className="icon-m fill-l-yellow" />
				</div>
			</button>
			<div ref={dropdownRef} className={`dropdown ${isActive ? 'active' : 'inactive'}`}>
				<ul>
					<li>
						<a href="/">
							Hoe kan jij...
						</a>
					</li>
					<li>
						<a href="/">
							Voor jou omgeving...
						</a>
					</li>
					<li>
						<a href="/">
							Betekenisvolle waarde creëeren.
						</a>
					</li>
				</ul>
			</div>
		</div>
	)
}

export default DropdownMenu
