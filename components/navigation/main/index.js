import React, { Fragment } from 'react'

// LIBRARIES
import Image from 'next/image'
import Link from 'next/link'
import { motion } from 'framer-motion'

// COMPONENTS



const MainNavigation = () => {
	return (
		<Fragment>
			<div className="main-nav-container">
				<div className="main-nav-inner flex flex-row flex-between align-middle">
					<div className="main-logo">
						<Link href="/">
							<a>
								<Image
									alt="YouLab logo"
									src="/images/logo-youlab.svg"
									layout="fixed"
									width="125px"
									height="25px"
									objectFit="fit"
									quality={80}
									priority={true}
								/>
							</a>
						</Link>
					</div>
					<Link href="/overzicht">
						<a>
							<motion.button className="button button-answer bg-black" whileHover={{ scale: 1.1 }}>
								Hoofdstukken
							</motion.button>
						</a>
					</Link>
				</div>
			</div>
		</Fragment>
	)
}

export default MainNavigation
