import React, { useState, useEffect } from "react";

const ImageBgLoader = ({ url = "", className = "" }) => {
    const [loaded, setLoaded] = useState(false);

    useEffect(() => {
        let img = new Image();
        img.onload = () => setLoaded(true);

        img.src = url;
        // eslint-disable-next-line
    }, []);

    return (
        <div
            className={`overlay img-background img-animated loading ${
                loaded ? "loaded" : ""
            } ${className}`}
            style={{ backgroundImage: `url('${loaded ? url : ""}')` }}
        ></div>
    );
};

export default ImageBgLoader
