import React, { useState, useEffect } from "react";

const ImageLoader = ({
    url,
    alt = "",
    width = "auto",
    height = "auto",
    className = "",
}) => {
    const [loaded, setLoaded] = useState(false);

    useEffect(() => {
        var img = new Image();
        img.onload = () => setLoaded(true);

        img.src = url;
        //  eslint-disable-next-line
    }, []);

    return (
        <img
            className={`img-animated ${
                loaded ? "loaded" : "loading"
            } ${className}`}
            width={width}
            height={height}
            alt={alt}
            src={url}
        ></img>
    );
};

export default ImageLoader;
