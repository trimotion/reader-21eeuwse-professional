import React, { useState, useEffect } from "react";

export default function useWindowSize() {
	
	const [windowSize, setWindowSize] = useState(0);

	useEffect(() => {
        window.addEventListener("resize", () => {
            setWindowSize({
                width: window.innerWidth,
                height: window.innerHeight,
            });
        });
    }, []);

    if (typeof window !== "undefined") {
        return { width: window.innerWidth, height: window.innerHeight };
    }
	
    return windowSize;
}
