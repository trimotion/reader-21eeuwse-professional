import React from "react";
import { useRouter } from "next/router";

export default function Layout({ children }) {
    const { isPreview } = useRouter();
    return (
        <div className="absolute exit-preview-btn">
            {children}
            {isPreview ? <a href="/api/exit-preview">Exit Preview</a> : null}
        </div>
    );
}
