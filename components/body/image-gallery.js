import React, { useState, useRef } from 'react'

// LIBRARIES
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext, Image, DotGroup, Dot } from 'pure-react-carousel';
import 'node_modules/pure-react-carousel/dist/react-carousel.es.css'
import { RichText } from 'prismic-reactjs'
import { motion, useDomEvent } from 'framer-motion'

// COMPONENTS
import { IconRight, IconLeft } from '@components/icons'

const ImageGallery = ({ title, description, items = []}) => {
	
	return (
		<div className="body-image-gallery">
			<div>
				<h1>
					{RichText.asText(title)}
				</h1>
				<p>
					{RichText.asText(description)}
				</p>
			</div>
			<CarouselProvider
				naturalSlideWidth={100}
				naturalSlideHeight={100}
				totalSlides={items.length}
				orientation="horizontal"
				isIntrinsicHeight={true}
				className="image-gallery-carousel"
			>
				<Slider
					className="image-gallery-slider"
				>
					{items.map(( item, i ) => {

						return (
							<Slide 
								index={i} 
								key={`post-${i}`}
							>
								<Image
									alt={item.image.alt}
									src={item.image.url}
									isBgImage={true}
									tag="div"
									className="image-gallery-slide"
								/>
								<div className="flex justify-center alt-image-text">
									<span>{item.image.alt}</span>
								</div>
							</Slide>
						)
					})}
					
				</Slider>
				<div className="flex flex-between align-middle relative image-gallery-controls">
					<ButtonBack className="button image-gallery-btn-back bg-black">
						<IconLeft className="icon-m fill-l-yellow" />
					</ButtonBack>
					<ButtonNext className="button image-gallery-btn-next bg-black">
						<IconRight className="icon-m fill-l-yellow" />
					</ButtonNext>
				</div>
				<DotGroup className="flex justify-center" />
			</CarouselProvider>
		</div>
	)
}

export default ImageGallery
