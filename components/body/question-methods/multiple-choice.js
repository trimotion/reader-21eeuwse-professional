import React, { Fragment, useState, useEffect } from 'react'
import { useRouter } from 'next/router'

// LIBRARIES
import { RichText } from 'prismic-reactjs'
import { motion } from 'framer-motion'

// COMPONENTS
import Checkbox from './utils/checkbox'

const MultipleChoice = ({ question, answer, items = [] }) => {
	const router = useRouter()

	const [hidden, setHidden] = useState(false)

	useEffect(() => {
		const handleRouteChange = (url, { shallow }) => {
			setHidden(false)
		}

		router.events.on('routeChangeStart', handleRouteChange)

		return () => {
			router.events.off('routeChangeComplete', handleRouteChange)
		  }
	}, [])

	return (
		<Fragment>
			<div className="">
				<div className="question-card">
					{hidden 
						? <AnswerBlock answer={answer} />
						: <QuestionBlock question={question} items={items} />
					}
					{hidden ? (
						null
					) : (
						<div className="flex justify-center">
							<motion.button className="button button-answer bg-black" whileHover={{ scale: 1.1 }} onClick={() => setHidden(true)}>
								Antwoorden
							</motion.button>
						</div>
					)}
				</div>
			</div>
		</Fragment>
	)
}

const QuestionBlock = ({ question, items = [] }) => {
	return (
		<div>
			<div>
				<h1>
					{RichText.asText(question)}
				</h1>
			</div>
			<div className="question-choice-container">
				{items.map((item, i) => {
					return (
						<div key={i} className="flex flex-row align-middle">
							<div>
								<Checkbox />
							</div>
							<span className="">{item.choice}</span>
						</div>
					)
				})}	
			</div>
		</div>
	)
}

const AnswerBlock = ({ answer }) => {
	return (
		<div>
			<div>
				<p>
					{RichText.asText(answer)}
				</p>
			</div>
		</div>
	)
}

export default MultipleChoice
