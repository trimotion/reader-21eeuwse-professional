// Libraries
import Link from "next/link";

export function CustomLink(type, element, content, children, index) {
    if (element.data.link_type !== "Document") {
        return (
            <a
                key={index}
                href={element.data.url}
                target="_blank"
                rel="noreferrer noopener"
            >
                {content}
            </a>
        );
    }
    return (
        <Link
            key={index}
            href={cardLinkResolver(element.data)}
            locale={setLocal(element)}
        >
            <a>{content}</a>
        </Link>
    );
}
