import React from 'react';

// Libraries
import { RichText } from "prismic-reactjs";

// Components
import htmlSerializer from "@components/utils/htmlSerializer";


const Text = ({ text, title, titleThemeColor }) => {

    return (
		<div className="body-text">
			<h1 style={{ color: `${titleThemeColor}` }}>
				{RichText.asText(title)}
			</h1>
			<RichText
				render={text}
				htmlSerializer={htmlSerializer}
			/>
        </div>
    );
};

export default Text;
