import React from 'react'

// LIBRARIES
import EmbedContainer from 'react-oembed-container';

const Video = ({ embed }) => {
	return (
		<div className="body-video">
			<EmbedContainer markup={embed.html} className="video-container">
				<div dangerouslySetInnerHTML={{ __html: embed.html }} />
			</EmbedContainer>
		</div>
	)
}

export default Video
