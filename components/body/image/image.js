import React, { useState, useRef } from 'react'

// LIBRARIES
import { motion, useDomEvent } from 'framer-motion'

// COMPONENTS
import Image from '@components/images/image'


const transition = {
	type: "spring",
	damping: 25,
	stiffness: 120
};



const PageImage = ({ image }) => {

	const [isOpen, setOpen] = useState(false);

	const WindowDoc = typeof window !== 'undefined' ? window : {};

	useDomEvent(useRef(WindowDoc), "scroll", () => isOpen && setOpen(false));

	return (
		<div className={`body-image lightbox-container ${isOpen ? "open" : ""}`}>
			<motion.div
				animate={{ opacity: isOpen ? 1 : 0 }}
				transition={transition}
				className="shade"
				onClick={() => setOpen(false)}
			/>
			<motion.div onClick={() => setOpen(!isOpen)} transition={transition} layout >
				<Image 
					url={image.url}
					alt={image.alt} 
					layout="fill"
        			objectFit="cover" 
				/>
			</motion.div>
			<div className="flex justify-center alt-image-text">
				<span>{image.alt}</span>
			</div>
		</div>
	)
}

export default PageImage
