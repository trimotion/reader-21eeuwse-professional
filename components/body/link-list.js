import React from 'react'

// LIBRARIES
import { RichText } from 'prismic-reactjs'

// COMPONENTS
import Link from 'next/link'

const LinkList = ({ title, items = [] }) => {
	return (
		<div className="body-link-list">
			<h1>
				{RichText.asText(title)}
			</h1>
			<ul>
				{items.map(( item, i ) => {
					
					return (
						<li key={i}>
							<Link href={item.link.url}>
								<a href={item.link.url} target={item.link.target}>
									<h3>
										{RichText.asText(item.linkTitle)}
									</h3>
								</a>
							</Link>
						</li>
					)
				})}
			</ul>
		</div>
	)
}

export default LinkList
