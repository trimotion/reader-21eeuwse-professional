

// COMPONENTS
import Text from './text'
import Image from './image/image'
import TextLeft from './text-and-image/text-left'
import TextRight from './text-and-image/text-right'
import Quote from './quote'
import ImageGallery from './image-gallery'
import Video from './video'
import LinkList from './link-list'
import TextList from './text-list'
import MultipleChoice from './question-methods/multiple-choice'

const Body = ({ content = [], titleThemeColor }) => {
	// console.log(content)
	const types = {
		TEXT: 'text_paragraph',
		IMAGE: 'image_container',
		TEXT_AND_IMAGE: 'text_image_section',
		QUOTE: 'quote_section',
		IMAGE_GALLERY: 'image_gallery',
		VIDEO: 'video_container',
		LINK_LIST: 'link_list',
		TEXT_LIST: 'text_list',
		MULTIPLE_CHOICE: 'question_multiple_choice'
	}

	return (
		content &&
		content.map(({slice_type, variation, primary = null, items = []}, index) => {
			switch (slice_type) {
				case types.TEXT:
					return <Text key={index} {...primary} titleThemeColor={titleThemeColor} />;
				case types.IMAGE:
					return <Image key={index} {...primary} />;
				case types.TEXT_AND_IMAGE:
					switch (variation) {
						case ('default-slice'):
							return <TextLeft key={index} {...primary} />;
						case ('right'):
							return <TextRight key={index} {...primary} />;
					}
				case types.QUOTE:
					return <Quote key={index} {...primary} />;
				case types.IMAGE_GALLERY:
					return <ImageGallery key={index} {...primary} items={items} />;
				case types.VIDEO:
					return <Video key={index} {...primary} />;
				case types.LINK_LIST:
					return <LinkList key={index} {...primary} items={items} />;
				case types.TEXT_LIST:
					return <TextList key={index} {...primary} items={items} />;
				case types.MULTIPLE_CHOICE:
					return <MultipleChoice key={index} {...primary} items={items} />;
			}
		})
	)
}

export default Body;