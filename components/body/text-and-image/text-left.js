import React from "react";

// LIBRARIES
import { RichText } from "prismic-reactjs";

// COMPONENTS
import htmlSerializer from "@components/utils/htmlSerializer";
import Image from "@components/images/image";

const TextLeft = ({ title = [], text = [], image }) => {
    return (
        <div className="body-text-image flex flex-row">
            <div>
                {title && <div>{RichText.asText(title)}</div>}
                <RichText render={text} htmlSerializer={htmlSerializer} />
            </div>
            <div>
                <Image url={image.url} width="100%" alt={image.alt} />
            </div>
        </div>
    );
};

export default TextLeft;
