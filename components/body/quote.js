import React from "react";

// COMPONENTS
import { RichText } from "prismic-reactjs";

const PageQuote = ({ quote, author }) => {
    return (
        <div className="body-quote">
            <div className="quote-text">{RichText.asText(quote)}</div>
            {author && (
                <i className="quote-author">- {RichText.asText(author)}</i>
            )}
        </div>
    );
};

export default PageQuote;
