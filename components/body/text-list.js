import React from 'react'

// LIBRARIES
import { RichText } from 'prismic-reactjs'

const TextList = ({ title, items = [] }) => {
	return (
		<div className="body-text-list">
			<h1>
				{RichText.asText(title)}
			</h1>
			<ul>
				{items.map(( item, i ) => {
					return (
						<li key={i}>
							{RichText.asText(item.listItem)}
						</li>
					)
				})}
			</ul>
		</div>
	)
}

export default TextList
