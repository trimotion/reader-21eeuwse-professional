import React, { Fragment } from 'react'

import Link from 'next/link'
import { motion } from 'framer-motion'

// COMPONENTS
import Image from 'next/image'

const CardCarousel = ({
	linkChapterUID,
	linkBlockChapterUID,
	cardThemeSwitch,
	titleNumber,
	title = [],
	description,
	src,
	alt
}) => {
	return (
		<Fragment>
			<div className="slide-card-container">
				<motion.div className="card-carousel" whileHover={{ scale: 1.02 }} initial={{ opacity: 0 }}
						animate={{ opacity: 1 }}
						exit={{ opacity: 0 }}>
					<Link
						href={
							`${linkChapterUID}` +
							"/" +
							`${linkBlockChapterUID}`
						}
						passHref
					>
						<div>
							<motion.div 
								initial={{ opacity: 0 }}
								animate={{ opacity: 1 }}
								exit={{ opacity: 0 }}
								className="flex flex-column card-carousel-img"
								style={{backgroundColor: `${cardThemeSwitch}`}}
							>
								<Image
									alt={alt}
									src={src}
									layout="responsive"
									width="500px"
									height="500px"
									objectFit="contain"
									quality={80}
									priority={true}
									className="center"
								/>
							</motion.div>
							<div className="card-carousel-text">
								<h1 className="card-carousel-title">
									{titleNumber}. {title}
								</h1>
								<br />
								<p className="card-carousel-subtitle">{description}</p>
							</div>
						</div>
					</Link>
				</motion.div>
			</div>
		</Fragment>
	)
}

export default CardCarousel