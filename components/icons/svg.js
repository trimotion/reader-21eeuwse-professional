import React from "react";

export const Svg = ({ children, className = "", style }) => (
    <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 24 24"
        fill="currentColor"
        stroke="none"
        strokeWidth="0"
        strokeLinecap="round"
        strokeLinejoin="round"
        className={`icon ${className}`}
		style={style}
    >
        {children}
    </svg>
);



