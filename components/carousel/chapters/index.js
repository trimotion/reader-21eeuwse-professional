import React, { useState, useContext, useEffect } from 'react'

// LIBRARIES
import { CarouselContext, Slider, Slide, ButtonBack, ButtonNext, WithStore } from 'pure-react-carousel'
import 'node_modules/pure-react-carousel/dist/react-carousel.es.css'
import { motion } from 'framer-motion'

// COMPONENTS
import { RichText } from 'prismic-reactjs'
import SlideContainer from './container'
import ProgressBar from './progress-bar'
import { IconRight, IconLeft } from '@components/icons'
import { yellowTheme, blueTheme, pinkTheme, purpleTheme } from '@components/themes'



const CarouselChapters = ({chapterList}) => {

	const carouselContext = useContext(CarouselContext);
	const [currentSlide, setCurrentSlide] = useState(carouselContext.state.currentSlide);
	
	useEffect(() => {
		function onChange() {
			setCurrentSlide(carouselContext.state.currentSlide);
		}
		carouselContext.subscribe(onChange);
		return () => carouselContext.unsubscribe(onChange);
	}, [carouselContext]);

	const progressWidth = 100 / chapterList.length
	const totalSlides = chapterList.length
	const progressTotal = progressWidth * (currentSlide + 1)

	const desktopNav = () => {
		return (
			<div className="flex flex-between align-middle slide-control-container-top" style={{zIndex: '999'}}>
				<motion.div whileHover={{ scale: 1.15 }}>
					<ButtonBack className="button button-back bg-black">
						<IconLeft className="icon-m fill-white" />
					</ButtonBack>
				</motion.div>
				<ProgressBar progressTotal={progressTotal} totalSlides={totalSlides} currentSlide={currentSlide} />
				<motion.div whileHover={{ scale: 1.15 }}>
					<ButtonNext className="button button-next bg-black">
						<IconRight className="icon-m fill-white" />
					</ButtonNext>
				</motion.div>
			</div>
		)
	}

	const mobileNav = () => {
		return (
			<div className="flex flex-between align-middle slide-control-container-bottom" style={{zIndex: '999'}}>
				<ButtonBack className="button button-back bg-black">
					<IconLeft className="icon-m fill-white" />
				</ButtonBack>
				<ProgressBar progressTotal={progressTotal} totalSlides={totalSlides} currentSlide={currentSlide} />
				<ButtonNext className="button button-next bg-black">
					<IconRight className="icon-m fill-white" />
				</ButtonNext>
			</div>
		)
	}

	return (
		<div className="slide-container">
			<div className="mb-off">
				{desktopNav()}
			</div>
			<div className="mb-on">
				{mobileNav()}
			</div>
			
			<Slider>
				{chapterList.map((item, i) => {
					const currentTheme = item.select_theme

					const bgThemeSwitch = () => {
						switch (currentTheme) {
							case 'yellow':
								return `${yellowTheme.backgroundColor}`
							case 'blue':
								return `${blueTheme.backgroundColor}`
							case 'pink':
								return `${pinkTheme.backgroundColor}`
							case 'purple':
								return `${purpleTheme.backgroundColor}`
						}
					}

					const cardThemeSwitch = () => {
						switch (currentTheme) {
							case 'yellow':
								return `${yellowTheme.cardColor}`
							case 'blue':
								return `${blueTheme.cardColor}`
							case 'pink':
								return `${pinkTheme.cardColor}`
							case 'purple':
								return `${purpleTheme.cardColor}`
						}
					}
					
					return (
						<Slide 
							index={i} 
							key={`post-${i}`}
							className="carousel-slide"
							style={{ backgroundColor: `${bgThemeSwitch()}`}}
						>	
							<SlideContainer 
								linkChapterUID={item.chapter.uid}
								linkBlockChapterUID={item.chapter_block.uid}
								cardThemeSwitch={cardThemeSwitch()}
								titleNumber={i + 1}
								title={RichText.asText(item.chapter_title)}
								description={item.chapter_description}
								src={item.chapter_image.url}
								alt={item.chapter_image.alt}
							/>
						</Slide>
					);
				})}
			</Slider>
		</div>
	)
}

export default CarouselChapters
