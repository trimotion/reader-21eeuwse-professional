import React, { Fragment } from "react";

// COMPONENTS
import CardCarousel from '@components/cards/card-carousel'

const SlideContainer = ({
    linkChapterUID,
    linkBlockChapterUID,
	cardThemeSwitch,
	titleNumber,
    title = [],
    description,
    src,
    alt,
}) => {
    return (
        <Fragment>
			<div>
				<CardCarousel
					linkChapterUID={linkChapterUID}
					linkBlockChapterUID={linkBlockChapterUID}
					cardThemeSwitch={cardThemeSwitch}
					titleNumber={titleNumber}
					title={title}
					description={description}
					src={src}
					alt={alt}
				/>
			</div>
        </Fragment>
    );
};

export default SlideContainer;
