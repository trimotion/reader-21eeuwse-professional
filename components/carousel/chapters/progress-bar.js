import React, { Fragment } from 'react'


const ProgressBar = ({progressTotal, totalSlides, currentSlide}) => {

	const thisSlide = currentSlide + 1

	return (
		<Fragment>
			<div className="slide-control-bar">
				<div className="progress-bar">
					<div className="progress-fill-carousel" style={{width: `${progressTotal}` + '%', position: 'relative', transition: '200ms ease-out'}}>
						<div className="progress-bar-tooltip" style={{ backgroundColor: `black`, color: 'white' }}>
							<div>
								<span style={{ transition: '200ms ease-out' }}>{thisSlide && thisSlide}</span> / {totalSlides && totalSlides}
							</div>
						</div>
					</div>
				</div>
			</div>
		</Fragment>
	)
}

export default ProgressBar
