export const yellowTheme = {
	backgroundColor: "#FFFCB3", // light-yellow
	titleColor: "#FFCC00", // dark-yellow
	buttonColor: "#FFFCB3",
	loadingColor: "#FFCC00",
	readingTimeColor: "#FFCC00",
	cardColor: "#FFCC00"
}

export const blueTheme = {
	backgroundColor: "#B8EEF9", // light-blue
	titleColor: "#45CBE5", // primary-blue
	buttonColor: "#B8EEF9",
	loadingColor: "#45CBE5",
	readingTimeColor: "#45CBE5",
	cardColor: "#45CBE5"
}


export const pinkTheme = {
	backgroundColor: "#FFE0E0", // secondary-pink
	titleColor: "#FFCCCC", // primary-pink
	buttonColor: "#FFE0E0",
	loadingColor: "#FFCCCC",
	readingTimeColor: "#FFCCCC",
	cardColor: "#FFCCCC"
}


export const purpleTheme = {
	backgroundColor: "#CAC2EF", // light-purple
	titleColor: "#150080", // primary-purple
	buttonColor: "#CAC2EF",
	loadingColor: "#150080",
	readingTimeColor: "#150080",
	cardColor: "#150080"
}


