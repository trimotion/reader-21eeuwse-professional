
import { motion } from 'framer-motion'

const childBox = {
    initial: {
        height: "100vh",
        top: 0,
    },
    animate: {
        height: 0,
		bottom: 0,
        transition: {
            when: "afterChildren",
			staggerChildren: 0.2,
            duration: 2,
            ease: [0.87, 0, 0.13, 1],
        },
    },
};


const InitialTransition = () => {
	return (
		<div style={{ position: "absolute", bottom: '0px', left: '0px', right: '0px'}}>
			<motion.div
				style={{ 
					position: "relative", 
					// display: "flex", 
					zIndex: "1010", 
					width: "100%", 
					backgroundColor: "#150080"
				}}
				initial="initial"
				animate="animate"
				variants={childBox}
			/>
			<motion.div
				style={{ 
					position: "relative", 
					// display: "flex", 
					zIndex: "1009", 
					width: "100%", 
					backgroundColor: "#FFCCCC" 
				}}
				initial="initial"
				animate="animate"
				variants={childBox}
			/>
			<motion.div
				style={{ 
					position: "relative", 
					// display: "flex", 
					zIndex: "1008", 
					width: "100%", 
					backgroundColor: "#45CBE5" 
				}}
				initial="initial"
				animate="animate"
				variants={childBox}
			/>
			<motion.div
				style={{ 
					position: "relative", 
					// display: "flex", 
					zIndex: "1007", 
					width: "100%", 
					backgroundColor: "#FFE500" 
				}}
				initial="initial"
				animate="animate"
				variants={childBox}
			/>
		</div>
	);
};

export default InitialTransition;