// Library
import Head from "next/head";
import { useRouter } from "next/router";

// COMPONENTS 
import { RichText } from 'prismic-reactjs'

const Header = ({ title, description, lang = "nl-nl" }) => {


    const { asPath } = useRouter();

    const url = `https://reader.youlab.nl${asPath}`;

    return (
        <Head>
            {/*
				// https://moz.com/blog/the-ultimate-guide-to-seo-meta-tags
				// This tag is necessary to declare your character set for the page and should be present on every page. 
			*/}

            <meta
                httpEquiv="Content-Type"
                content="text/html; charset=utf-8"
            />

            {/*
				// https://moz.com/blog/the-ultimate-guide-to-seo-meta-tags
				// In this mobile world, you should be specifying the viewport. If you don’t, you run the risk of having a poor mobile experience
			*/}
            <meta
                name="viewport"
                content="width=device-width,initial-scale=1,shrink-to-fit=no,viewport-fit=cover"
            />

            {/*
				// https://moz.com/learn/seo/meta-description
				// The meta description is an HTML attribute that provides a brief summary of a web page. 
				// Search engines such as Google often display the meta description in search results, which can influence click-through rates.
			*/}
            <title>{RichText.asText(title) + ` - 21e eeuwse professional`}</title>
            <meta name="description" content={description} />
            <meta
                name="image"
                content="/socialmedia-cards/socialmedia-card-image-google-1200x630.png"
            />
            {/*
				// https://fonts.google.com/
				// Google fonts with async loading for non blocking fast page loading
			*/}

            {/* OG tags card */}
            <meta property="og:locale" content={lang} />
            <meta property="og:type" content="website" />

            <meta property="og:title" content={RichText.asText(title)} />
            <meta property="og:description" content={description} />

            <meta property="og:url" content={url} />
            <meta
                property="og:site_name"
                content="TriMotion"
            />

            <meta
                property="og:image"
                content="/socialmedia-cards/socialmedia-card-image-google-1200x630.png"
            />
            <meta property="og:image:width" content="1200" />
            <meta property="og:image:height" content="630" />

            <meta
                property="og:image:alt"
                content="Social media card 21e eeuwse professional"
            ></meta>

            {/* Twitter card */}
            <meta name="twitter:card" content="summary_large_image"></meta>
            <meta name="twitter:site" content="@TRIMOTION"></meta>
            <meta name="twitter:creator" content="@TRIMOTION"></meta>

            <meta name="twitter:title" content={RichText.asText(title)} />
            <meta name="twitter:description" content={description} />
            <meta
                name="twitter:image"
                content="/socialmedia-cards/socialmedia-card-image-twitter-600x400.png"
            />
            <meta
                name="twitter:image:alt"
                content="Social media card 21e eeuwse professional"
            />
            <meta name="twitter:url" content={url} />

            {/*
				// https://favicon.io/
				// Create favicons and meta data for them   // <link rel="manifest" href="../../assets/favicon/site.webmanifest" />
			*/}
            <meta name="theme-color" content="#150080" />
            <link
                rel="apple-touch-icon"
                sizes="180x180"
                href="/favicon/apple-touch-icon.png"
            />
            <link
                rel="icon"
                type="image/png"
                sizes="32x32"
                href="/favicon/favicon-32x32.png"
            />
            <link
                rel="icon"
                type="image/png"
                sizes="16x16"
                href="/favicon/favicon-16x16.png"
            />
        </Head>
    );
};

export default Header;
